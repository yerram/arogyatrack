import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registration',
    loadChildren: () => import('./registration/registration.module').then( m => m.RegistrationPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },  
  {
    path: 'medical-visit',
    loadChildren: () => import('./menu/medical-visit/medical-visit.module').then( m => m.MedicalVisitPageModule)
  },
  {
    path: 'new-visit',
    loadChildren: () => import('./menu/medical-visit/medical-visit.module').then( m => m.MedicalVisitPageModule)
  },
  {
    path: 'previouse-visit',
    loadChildren: () => import('./menu/medical-visit/medical-visit.module').then( m => m.MedicalVisitPageModule)
  },
  {
    path: 'blood-pressure',
    loadChildren: () => import('./menu/blood-pressure/blood-pressure.module').then( m => m.BloodPressurePageModule)
  },
  {
    path: 'blood-glucose',
    loadChildren: () => import('./menu/blood-glucose/blood-glucose.module').then( m => m.BloodGlucosePageModule)
  },
  {
    path: 'weight',
    loadChildren: () => import('./menu/weight/weight.module').then( m => m.WeightPageModule)
  }, 
  {
    path: 'medical-history',
    loadChildren: () => import('./menu/medical-history/medical-history.module').then( m => m.MedicalHistoryPageModule)
  },
  {
    path: 'medical-records',
    loadChildren: () => import('./menu/medical-records/medical-records.module').then( m => m.MedicalRecordsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
