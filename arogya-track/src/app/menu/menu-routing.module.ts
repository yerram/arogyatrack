import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage
  },
  {
    path: 'medical-history',
    loadChildren: () => import('./medical-history/medical-history.module').then( m => m.MedicalHistoryPageModule)
  },
  {
    path: 'medical-visit',
    loadChildren: () => import('./medical-visit/medical-visit.module').then( m => m.MedicalVisitPageModule)
  },
  {
    path: 'blood-pressure',
    loadChildren: () => import('./blood-pressure/blood-pressure.module').then( m => m.BloodPressurePageModule)
  },
  {
    path: 'blood-glucose',
    loadChildren: () => import('./blood-glucose/blood-glucose.module').then( m => m.BloodGlucosePageModule)
  },
  {
    path: 'weight',
    loadChildren: () => import('./weight/weight.module').then( m => m.WeightPageModule)
  },
  {
    path: 'medical-records',
    loadChildren: () => import('./medical-records/medical-records.module').then( m => m.MedicalRecordsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
