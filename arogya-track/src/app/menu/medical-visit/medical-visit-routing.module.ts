import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MedicalVisitPage } from './medical-visit.page';

const routes: Routes = [
  {
    path: '',
    component: MedicalVisitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MedicalVisitPageRoutingModule {}
