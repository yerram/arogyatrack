import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MedicalVisitPageRoutingModule } from './medical-visit-routing.module';

import { MedicalVisitPage } from './medical-visit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MedicalVisitPageRoutingModule
  ],
  declarations: [MedicalVisitPage]
})
export class MedicalVisitPageModule {}
