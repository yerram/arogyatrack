import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BloodGlucosePageRoutingModule } from './blood-glucose-routing.module';

import { BloodGlucosePage } from './blood-glucose.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BloodGlucosePageRoutingModule
  ],
  declarations: [BloodGlucosePage]
})
export class BloodGlucosePageModule {}
