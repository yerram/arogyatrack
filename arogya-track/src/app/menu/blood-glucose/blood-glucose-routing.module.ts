import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BloodGlucosePage } from './blood-glucose.page';

const routes: Routes = [
  {
    path: '',
    component: BloodGlucosePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BloodGlucosePageRoutingModule {}
